%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Overleaf (WriteLaTeX) Example: Molecular Chemistry Presentation
%
% Source: http://www.overleaf.com
%
% In these slides we show how Overleaf can be used with standard 
% chemistry packages to easily create professional presentations.
% 
% Feel free to distribute this example, but please keep the referral
% to overleaf.com
% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% How to use Overleaf: 
%
% You edit the source code here on the left, and the preview on the
% right shows you the result within a few seconds.
%
% Bookmark this page and share the URL with your co-authors. They can
% edit at the same time!
%
% You can upload figures, bibliographies, custom classes and
% styles using the files menu.
%
% If you're new to LaTeX, the wikibook is a great place to start:
% http://en.wikibooks.org/wiki/LaTeX
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\documentclass{beamer}

% For more themes, color themes and font themes, see:
% http://deic.uab.es/~iblanes/beamer\_gallery/index\_by\_theme.html
%
\mode<presentation>
{
  \usetheme{Madrid}       % or try default, Darmstadt, Warsaw, ...
  \usecolortheme{default} % or try albatross, beaver, crane, ...
  \usefonttheme{serif}    % or try default, structurebold, ...
  \setbeamertemplate{navigation symbols}{}
  \setbeamertemplate{caption}[numbered]
} 

\usepackage[english]{babel}
\usepackage[utf8x]{inputenc}
\usepackage{chemfig}
\usepackage[version=3]{mhchem}
\usepackage{tikz}
\usepackage{mathrsfs}
\usepackage{graphics,graphicx,picture}
\usepackage{setspace}
\usepackage{multirow}
\usepackage{amsmath}
% On Overleaf, these lines give you sharper preview images.
% You might want to `comment them out before you export, though.
\usepackage{pgfpages}
\pgfpagesuselayout{resize to}[%
  physical paper width=8in, physical paper height=6in]
\makeatletter
\newcommand{\rmnum}[1]{\romannumeral #1}
\newcommand{\Rmnum}[1]{\expandafter\@slowromancap\romannumeral #1@}
\newcommand{\Mypm}{\mathbin{\tikz [x=1.4ex,y=1.4ex,line width=.1ex] \draw (0.0,0) -- (1.0,0) (0.5,0.08) -- (0.5,0.92) (0.0,0.5) -- (1.0,0.5);}}%
\newcommand{\mypm}{\mathbin{\mathpalette\@mypm\relax}}
\newcommand{\@mypm}[2]{\ooalign{%
  \raisebox{.1\height}{$#1+$}\cr
  \smash{\raisebox{-.6\height}{$#1-$}}\cr}}
\makeatother

% Here's where the presentation starts, with the info for the title slide
\title{Hypothesis Testing V}
\author{Prof. Mark Brown}
\date{December 5, 2016}

\begin{document}

\begin{frame}
  \titlepage
\end{frame}

%%slide 2
\begin{frame}{Non-Parametric Tests}
\underline{Non-Parametric Tests}\\
\vspace{0.3cm}
Non-parametric statistics refers to statistical inference without the assumption that the unknown distribution belongs to a parametric family.\\
\vspace{0.3cm}
Here is an example. We have two independent samples, $X_1,\cdots,X_n,$ $ Y_1,\cdots,Y_m$ and we wish to test the null hypothesis that their true distributions, $F$ for $X_1,\cdots,X_n$, $G$ for $Y_1,\cdots,Y_m$, are identical. A one-sided alternative is that $F$ is stochastically larger than $G$ meaning,
$$\bar{F}(t) \geq \bar{G}(t) \quad \text{for all }t, $$
with inequality for some t. We assume that $F$ and $G$ are continuous. That avoids dealing with tied observations. However there are satisfactory methods for dealing with ties. 
\end{frame}

%%slide 3
\begin{frame}{Non-Parametric Tests}
Thus we have,
$$H_0:F=G \quad vs \quad H_1:F \overset{st}{\geq} G \text{(F stochastically greater than G)}$$
with $F$ and $G$ continuous. Note that we don't treat $F$ and $G$ as normally distributed; nor we employ any other parametric family.\\
\vspace{0.3cm}
The following procedure was introduced by Wilcoxon in the early 1950's. To illustrate, suppose that n=6 and m=5. The observations are:
$$I: 65.2,67.1,69.4,78.2,74,83$$
$$II: 59.4,72.1,68,66.4,54.5$$
Rank the observations in the combined samply from smallest to largest giving ranks $1,\cdots,n+m$.
$$I \text{ Ranks: } 3,5,7,10,9,11$$
$$II \text{ Ranks: } 2,8,6,4,1$$

\end{frame}

%%slide 4
\begin{frame}{Non-Parametric Tests}
Notice that the ranks in group I tend to be higher than in group II. If $H_0$ were true then all $\left(_6^{11} \right)=462$ subset of size 6 from $\{1,\cdots,11\}$ would be equally likely as the set of ranks for the group I observations.\\
\vspace{0.8cm}
Define $W$, the Wilcoxon statistic to be the sum of the group 1 ranks; in this example $W=3+5+7+10+9+11=45$. The idea is that if $W$ is unusually large under $H_0$ then either it is due to chance, or it is due to $H_0$ being false in the direction of $F$ being larger than $G$. In this example the P value corresponding to $W=45$ equals,
$$\frac{\{\text{\# subsets of size 6 from }\{1,\cdots,11\} \text{ with } W \geq 45 \}}{462}$$
We enumerate:



\end{frame}

%%slide 5
\begin{frame}{Non-Parametric Tests}
\begin{spacing}{1.3}
$$ 
\begin{tabular}{c|c|c |c}
$\text{5 Highest ranks}$ & $\text{$6^{th}$ rank}$ & $\text{\# subsets}$ & W\\
11,10,9,8,7 & 1-6  &  6  &46-51\\
 11,10,9,8,6& 1-5  &   5 &45-49\\
11,10,9,8,5  & 2-4  &   3 & 45-47\\
11,10,9,8,4   & 3  &   1 & 45\\
11,10,9,7,6    & 2-5  & 4 & 45-48\\
11,10,9,7,5     &  3-4 & 2 & 45-46\\
11,10,9,6,5      &  4 &   1 &45\\
11,10,8,7,6       & 3-5  & 3 & 45-47\\
 11,10,8,7,5       & 4  &   1  &45\\
 11,9,8,7,6        &  4-5 &  2  & 45-46\\
 10,9,8,7,6         & 5  &   1  &45\\

\end{tabular} 
$$
\end{spacing} 

\end{frame}

%%slide 6
\begin{frame}{Non-Parametric Tests}
P value=(Total \# of subsets of subsets of size 6 from $\{1,\cdots,11\}$ with $W \geq 45$)/462
$$=\frac{29}{462}=0.0628$$
We are thus led to doubting that $H_0$ is true, however we accept $H_0$ at the 0.05 level of significance, rejecting it for example, at 0.10 level.\\
\vspace{0.3cm}
For large values of n, m, enumeration is difficult. However the normal approximation with continuity correction is quite accurate. Define,
$$\delta_i=
\left\{
\begin{array}{ll}
1, \quad \text{if $i^{th}$ ranking observation from group I}\\
0,\quad  \text{if group II}\\
\end{array}
\right.
$$
Then, $W=\sum\limits_{i=1}^{n+m} i \delta_i,$ and

\end{frame}

%%slide 7
\begin{frame}{Non-Parametric Tests}
$$E_{H_0} W= \sum\limits_{i=1}^{n+m} i E_{H_0}(\delta_i)= \sum\limits_{i=1}^{n+m} i (\frac{n}{n+m})=\frac{n}{n+m} \sum\limits_{i=1}^{n+m}i$$
$$=\frac{n}{n+m} \frac{(n+m)(n+m+1)}{2}=\frac{n}{2}(n+m+1)$$

In our example, n=6, m=5 and
$$E_{H_0} W =\frac{6(6+5+1)}{2}=36$$
Next,
$$Var_{H_0} W=\sum\limits_1^{n+m} i^2 Var_{H_0} \delta_i +2\sum\limits_{1 \leq i <j \leq n+m} ijCov(\delta_i,\delta_j)$$
$$(1) =(Var_{H_0} \delta_1) \sum\limits_1^{n+m} i^2 +2Cov(\delta_1,\delta_2) \sum\limits_{1 \leq i<j \leq n+m} ij$$

\end{frame}

%%slide 8
\begin{frame}{Non-Parametric Tests}
(2) Next,
$$Var_{H_0}(\delta_i)=Var_{H_0} (\delta_1)=\frac{n}{n+m} (1-\frac{n}{n+m})=\frac{mn}{(n+m)^2}$$
while for $i \neq j$,
$$Cov_{\delta_i,\delta_j}=Cov(\delta_1,\delta_2)=E_{H_0}(\delta_1 \delta_2)-(E_{H_0} \delta_1)^2$$
$$(3) = \frac{n}{n+m} \frac{n-1}{n+m-1}-\frac{n}{(n+m)^2} =-\frac{nm}{(n+m)^2 (n+m-1)}$$
Now,
$$(4) \quad \sum\limits_1^{n+m} i^2 =\frac{(n+m)(n+m+1)(2n+2m+1)}{6}, \text{ and }$$

\end{frame}

%%slide 9
\begin{frame}{Non-Parametric Tests}
$$(5) \quad 2\sum\limits_{1 \leq i<j \leq n+m} ij=(\sum\limits_1^{m+n} i)^2 -\sum\limits_1^{m+n} i^2 $$
$$ =\frac{(n+m)(n+m+1)}{12}[3(n+m)(n+m+1)-2(2n+2m+1)]$$
$$=\frac{(n+m)(n+m+1)}{12}[3(n+m)+2][n+m-1]$$

From (1), (2), (3), (4) and (5) we finally compute,
$$(6) \quad Var_{H_0} W=\frac{nm(n+m+1)}{12}$$
In our example, $n=6,m=5$,
$$Var_{H_0} W=\frac{6(5)(12)}{12}=30$$

\end{frame}

%%slide 10
\begin{frame}{Comments}
Under $H_0, W$ has mean 36, variance 30, and can be shown to be approximately normal(using an appropriate version of the central limit theorem). Then with the continuity correction, the approximate P value equals,
$$P_{H_0}(W\geq 45) \approx 1-\Phi(\frac{44.5-36}{\sqrt{30}}) =0.0603,$$
which is reasonably close to the true P value, 0.0628. For large values of m,n when enumeration is difficult the normal approximation will be no harder to calculate than for small m,n, and will even be more accurate then in our small sample size example.\\
\vspace{0.1cm}
\underline{Comments}\\
\vspace{0.1cm}
1) Non-mathematically oriented people like the Wilcoxon test. It is easier to understand than the various parametric tests.\\
\vspace{0.1cm}
2) Because the Wilcoxon test does not make parametric assumptions, it is a valid test under a wider range of circumstances.\\

\end{frame}

%%slide 11
\begin{frame}{Comments}

3) Even if the parametric assumptions lending to a competing procedure, (for example a two sample t test), hold, the Wilcoxon test is not much worse in terms of power as measured by a test property known as Pitman efficiency.\\
\vspace{0.2cm}
For a 2 sided test $H_0:F=G \; vs \; H_1:F\neq G$ we reject $H_0$ for W too large or too small. If we observe $W=k$ then the P value equals, $2min(P_{H_0}(W \leq k), P_{H_0}(W \geq k))$.\\
\vspace{0.2cm}
5) A P value can be approximated by simulation. Simulate a sample of size n chosen without replacement from the numbers $\{1,\cdots,m+n\}$. For this sample compute W, and call it $W_1$. Repeat this a large number of times. This gives a sample $W_1,\cdots,W_N$, iid, with $W_i$ distributed as W under $H_0$. Then the estimated P value for the 2 sided test equals,
$$2\;max[\frac{\# W_i \leq k}{N}, \; \frac{\# W_i \geq k}{N}],$$
where k is the observed value of W.\\

\end{frame}

%%slide 12
\begin{frame}{Example 2}

\underline{Example 2} $n=7,m=10,W=86$. Find the approximate P value for the 2-sided Wilcoxon test.\\
\vspace{0.3cm}
\underline{Solution}
$$E_{H_0} W=\frac{n(n+m+1)}{2}=\frac{7(18)}{2}=63$$
$$Var_{H_0} W=\frac{nm(n+m+1)}{12} =\frac{7(10)(18)}{12}=105$$
and, $P_{H_0}(W \geq 86) \approx 1-\Phi(\frac{85.5-63}{\sqrt{105}}) \approx 0.0141,$ while $P_{H_0}(W \leq 86)$ is much larger. Thus,
$$P=2\;min[P(W \leq 86), P(W \geq 86)]=2P(W \geq 86) \approx 0.0281$$
We thus would reject $H_0$ at the $\alpha=0.05$ level(for example) and conclude that $F \neq G$, and more specifically that $ F \overset{st}{\geq} G.$\\

\end{frame}

%%slide 13
\begin{frame}{Example 3}
\underline{Example 3} For $n=m=3$ find the exact distribution of W.\\
\vspace{0.3cm}
\underline{Solution} W ranges from 6(1,2,3), to 15(4,5,6). Moreover,
$$(7) \quad P_{H_0}(W=k)=P_{H_0}(W=21-k)$$
To prove (7), replace rank j by n+m+1-j, j being the $j^{th}$ smallest and n+m+1-j the $j^{th}$ largest rank. Now,
$$\sum\limits_{Gp1} [\text{ranks, smallest to largest}] \overset{H_0}{\sim} \sum\limits_{Gp1} [\text{ranks, largest to smallest}]$$
Thus, $(\delta^*(i)=1 \text{ if $i^{th}$ largest in combined sample is from group 1})$,
$$W=\sum\limits i \delta(i) \overset{H_0}{\sim} \sum\limits (n+m+1-i) \delta^*(i)$$
$$\sim n(n+m+1)-\tilde{W} \; (\tilde{W} \sim W)$$


\end{frame}

%%slide 14
\begin{frame}{Example 3}
 Thus,
$$P_{H_0}(W=k)=P_{H_0}(n(n+m+1)-W=k)=P_{H_0}(W=n(n+m+1)-k)$$
When $n=m=3,n(n+m+1)=21$ and (7) follows. Thus,
$$P_{H_0}(W=6)=P_{H_0}(W=15)=\frac{1}{20}; \; (1,2,3)$$
$$P_{H_0}(W=7)=P_{H_0}(W=14)=\frac{1}{20}; \; (1,2,4)$$
$$P_{H_0}(W=8)=P_{H_0}(W=13)=\frac{2}{20}; \; (1,2,5),(1,3,4)$$
$$P_{H_0}(W=9)=P_{H_0}(W=12)=\frac{3}{20}; \; (1,2,6),(1,3,5),(2,3,4)$$
$$P_{H_0}(W=10)=P_{H_0}(W=11)=\frac{3}{20}; \; (1,3,6),(1,4,5),(2,3,5)$$

\end{frame}


%%slide 15
\begin{frame}{Example 3}
Note that for testing $H_0: F=G \; vs \; H_1: F \overset{st}{\geq}G,$ with n=m=3:
\begin{spacing}{1.3}
$$ 
\begin{tabular}{c|c}
W & Pvalue\\
15 &1/20 \\
 14& 2/20\\
13  & 4/20\\
 12  & 7/20\\
  11  & 10/20\\
  10   & 13/20\\
    9  & 16/20\\
     8  & 18/20\\
      7  & 19/20\\
       6  & 20/20 \\
\end{tabular} 
$$
\end{spacing} 

\end{frame}
%%slide 16
\begin{frame}{Example 4}
For $W=15$, the approximate P-value under a normal approximation with continuity correction:
$$E_{H_0}W=\frac{n(n+m+1)}{2}=\frac{3(7)}{2}=10.5$$
$$Var_{H_0} W=\frac{nm(n+m+1)}{12}=\frac{9(7)}{12}=5.25$$
$$P_{H_0}(W\geq 15) \approx 1-\Phi(\frac{14.5-10.5}{\sqrt{5.25}}) \approx 0.0404$$
\underline{Example 4} $n=m=5.$ The observations are:
$$I \quad -0.7259 \; -0.2078 \;\; 1.0172 \; -0.7645 \;\; 0.1960$$
$$II \quad\quad   0.6067 \quad 1.6288 \quad 1.3900 \quad 2.6649 \quad 0.9896$$
$$H_0:F=G \quad vs \quad H_1: F\neq G$$
(i) For the 2 sided Wilcoxon test find exact and approximate P-values.\\


\end{frame}


%%slide 17
\begin{frame}{Example 4}
(ii) Now find the P-value under a 2-sided t test: $X_1,\cdots,X_5, N(\mu_1,\sigma^2);$ $ Y_1,\cdots,Y_5,N(\mu_2,\sigma^2)$. $\mu_1,\mu_2,\sigma^2$ unknown. $H_0:\mu_1=\mu_2\; vs \; H_1:\mu_1 \neq $ $ \mu_2$.\\
\vspace{0.3cm}
\underline{Solution} (i) The group 1 ranks are 1,2,3,4,7. Thus W=17.
\begin{spacing}{1.3}
$$ 
\begin{tabular}{cccc}
$\text{lowest 4 ranks}$ & $5^{th}$ lowest & \#subsets & W\\
1 2 3 4 &5-7 &3 & 15-17\\
 1 2 3 5 & 6 &1 &17 \\
\end{tabular} 
$$
\end{spacing} 

Total number of subsets with $W \leq 17$ equals 4.
$$Pvalue=2\;min(P_{H_0}(W\geq 17),P_{H_0}(W \leq 17))=2Pr(W \leq 17)$$
$$\frac{2(4)}{\left(_5^{10} \right)}=\frac{8}{252}=\frac{2}{63} \approx 0.0317$$

\end{frame}

%%slide 18
\begin{frame}{Example 4}
Approximate P-value: $$E_{H_0} W=27.5, Var_{H_0}W=\frac{275}{12}. Pvalue \approx 2\Phi(\frac{17.5-27.5}{\sqrt{275/12}})=0.0368$$
\vspace{0.3cm}
(ii) $\bar{x}=-0.09, S_x=0.73738, \bar{y}=1.456, S_y=0.78019$
$$S_p=\sqrt{\frac{4S_x^2+4S_y^2}{8}}=\sqrt{\frac{S_x^2+S_x^2}{2}}=0.75906$$
$$t_8 =\frac{|\bar{X}-\bar{Y}|}{S_p \sqrt{\frac{1}{n}+\frac{1}{m}}}=\frac{1.5547}{(0.75906) \sqrt{0.4}}=3.2385$$
$$Pvalue \approx 2P(t_8 >3.2385) =0.0119$$
The rejection of $F=G$ is stronger under the t test but the t test adds in the assumptions of normality and equality of variance.


\end{frame}


\end{document}





 