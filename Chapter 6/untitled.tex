%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Overleaf (WriteLaTeX) Example: Molecular Chemistry Presentation
%
% Source: http://www.overleaf.com
%
% In these slides we show how Overleaf can be used with standard 
% chemistry packages to easily create professional presentations.
% 
% Feel free to distribute this example, but please keep the referral
% to overleaf.com
% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% How to use Overleaf: 
%
% You edit the source code here on the left, and the preview on the
% right shows you the result within a few seconds.
%
% Bookmark this page and share the URL with your co-authors. They can
% edit at the same time!
%
% You can upload figures, bibliographies, custom classes and
% styles using the files menu.
%
% If you're new to LaTeX, the wikibook is a great place to start:
% http://en.wikibooks.org/wiki/LaTeX
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\documentclass{beamer}

% For more themes, color themes and font themes, see:
% http://deic.uab.es/~iblanes/beamer\_gallery/index\_by\_theme.html
%
\mode<presentation>
{
  \usetheme{Madrid}       % or try default, Darmstadt, Warsaw, ...
  \usecolortheme{default} % or try albatross, beaver, crane, ...
  \usefonttheme{serif}    % or try default, structurebold, ...
  \setbeamertemplate{navigation symbols}{}
  \setbeamertemplate{caption}[numbered]
} 

\usepackage[english]{babel}
\usepackage[utf8x]{inputenc}
\usepackage{chemfig}
\usepackage[version=3]{mhchem}
\usepackage{tikz}
\usepackage{mathrsfs}
\usepackage{graphics,graphicx,picture}
\usepackage{setspace}
\usepackage{multirow}
\usepackage{amsmath}
% On Overleaf, these lines give you sharper preview images.
% You might want to `comment them out before you export, though.
\usepackage{pgfpages}
\pgfpagesuselayout{resize to}[%
  physical paper width=8in, physical paper height=6in]
\makeatletter
\newcommand{\rmnum}[1]{\romannumeral #1}
\newcommand{\Rmnum}[1]{\expandafter\@slowromancap\romannumeral #1@}
\newcommand{\Mypm}{\mathbin{\tikz [x=1.4ex,y=1.4ex,line width=.1ex] \draw (0.0,0) -- (1.0,0) (0.5,0.08) -- (0.5,0.92) (0.0,0.5) -- (1.0,0.5);}}%
\newcommand{\mypm}{\mathbin{\mathpalette\@mypm\relax}}
\newcommand{\@mypm}[2]{\ooalign{%
  \raisebox{.1\height}{$#1+$}\cr
  \smash{\raisebox{-.6\height}{$#1-$}}\cr}}
\makeatother

% Here's where the presentation starts, with the info for the title slide
\title{Confidence Interval}
\author{Prof. Mark Brown}
\date{October 31, 2016}

\begin{document}

\begin{frame}
  \titlepage
\end{frame}

%%slide 2
\begin{frame}{Central Limit Theorem}
\underline{Confidence Intervals}\\
\vspace{0.3cm}
Suppose that $X_1,\cdots,X_n,\cdots$ is an iid sequence from a distribution with mean $\mu$, and variance $\sigma^2$. Consider the sample mean, $\bar{X}_n$. We know from our previous work that $E\bar{X}_n=\mu$. When an estimator of a parameter has it mean equal to the parameter, we say that it is an unbiased estimator. Thus the sample mean is an unbiased estimator of the true mean. The variance of the sample mean, $Var(\bar{X}_n)$ equals $\frac{VarX}{n}=\frac{\sigma^2}{n}$. The variance decreases with n. As we noted earlier using Chebichev's inequality, for any $\epsilon >0$,\\
\vspace{0.3cm}
(1) $\underset{n \rightarrow \infty}{lim} Pr(|\bar{X}_n-\mu|>\epsilon)=0$\\
\vspace{0.3cm}
Thus the sample mean converges to the true mean. Result(1) is called the weak law of large numbers(A stronger result which we won't be using is called the strong law of large numbers).\\
\vspace{0.3cm}

\end{frame}

%%slide 3
\begin{frame}{Continuity Correction}
In the case that $X\sim N(\mu,\sigma^2)$, closure of normality under linear combinations gives us,
$$\bar{X}_n\sim N(\mu,\sigma^2/n)$$
In building up results on confidence intervals we'll start with the unrealistic case that $X_1,\cdots,X_n$ are iid $N(\mu,\sigma_0^2)$, where the variance $\sigma^2$ is known, and equal to $\sigma_0^2.$\\
\vspace{0.1cm}
Define $Z_{\alpha}=\gamma_{1-\alpha}$ where $\gamma_p$ is the $p^{th}$ percentile from $N(0,1)$. Thus $\Phi(Z_{\alpha})=1-\alpha$ and $1-\Phi(Z_{\alpha})=Pr(Z>Z_{\alpha})=\alpha$. $Z_{\alpha}$ is known as the upper $\alpha$ percentile, meaning that the probability of exceeding $Z_{\alpha}$ equals $\alpha$. We will derive,
$$(2) Pr(\mu \in \bar{X}_n \pm \frac{\sigma_0 Z_{\alpha/2}}{\sqrt{n}})=1-\alpha$$
We call $\bar{X}_n \pm \frac{\sigma_0 Z_{\alpha/2}}{\sqrt{n}} $ a $100(1-\alpha)\%$ confidence interval for $\mu$. For example if $\alpha=0.05$, then $Z_{\alpha/2}=1.960$ to 3 decimal place and $\bar{X}_n \pm \frac{ 1.96 \sigma_0 }{\sqrt{n}} $
is a $95\%$ confidence interval for $\mu.$ 

\end{frame}

%%slide 4
\begin{frame}{Continuity Correction}
Note that in (2) $\mu$ is a fixed but unknown number while $\bar{X}_n \pm \frac{\sigma_0 Z_{\alpha/2}}{\sqrt{n}} $ is a random interval. If we apply this procedure over and over again, then in the long run the random interval will contain the true mean 95\% of the time.\\
\vspace{0.3cm}
\underline{Example} $n=10,\; \sigma_0=1,\; \alpha=0.05.$ Data: 4.69, 4.07, 5.50, 3.60,4.98, 6.02, 4.54, 3.8, 6.8, 4.98. Then
$$\bar{X}_{10} =4.898 \text{ and } \bar{X}_{10} \pm \frac{1.96 \sigma_0}{\sqrt{10}}=4.898 \pm 0.620=(4.278,5.518)$$
is over 95\% confidence interval for $\mu$.\\
\vspace{0.3cm}
If we give up the normality assumption but still have $X_1,\cdots,X_n$ iid with unknown mean $\mu$ and known variance $\sigma_0^2$ then (2) is an approximate confidence interval for $\mu$, for n large, by the central limit theorem(as $\bar{X}_n$ is asymptotically normal).\\

\end{frame}

%%slide 5
\begin{frame}{Continuity Correction}

\underline{Sample Variance}
Let $X_1,\cdots,X_n$ be iid with mean $\mu$ and variance $\sigma^2$. Then, since $E(\frac{X_i-\mu}{\sigma})^2=1$, it follows that,
$$(3) \quad E[\frac{1}{n} \sum\limits_1^n (X_i-\mu)^2]=\sigma^2$$
Thus (3) would give us an unbiased estimator of $\sigma^2$ if $\mu$ was known. If $\mu$ is unknown we can't compute the estimator (3). This leads to the idea replacing $\mu$ by $\bar{X}_n$, but to have the resulting estimator be unbiased we adjust n to n-1. The statistic,
$$S^2=\frac{1}{n-1} \sum\limits_1^n (X_i-\bar{X})^2$$
is known as the sample variance(Section 6.4). $S=\sqrt{S^2}$ is called the sample standard deviation. Note that,

\end{frame}

%%slide 6
\begin{frame}{Example}
(i)
$$\sum (X_i-\bar{X})^2=\sum X_i^2-2(\frac{\sum X_i}{n})(\sum X_i)+n(\frac{\sum X_i}{n})^2 $$
$$=\sum X_i^2 -\frac{1}{n} (\sum X_i)^2=(\sum X_i^2)-n \bar{X}^2$$
(ii)
$$E\sum X_i^2=nEX^2 =n(VarX+(EX)^2)=n(\sigma^2 +\mu^2)$$
and (iii)
$$nE\bar{X}^2=n[Var\bar{X}+(E\bar{X})^2]=n[\frac{\sigma^2}{n}+\mu^2]=\sigma^2+n\mu^2$$
From (i), (ii) and (iii)
$$E[\sum\limits_1^n (X_i -\bar{X})^2]=(n-1)\sigma^2$$

\end{frame}

%%slide 7
\begin{frame}{Example}
thus,
$$ES^2=\frac{1}{n-1}[(n-1)\sigma^2]=\sigma^2$$
which demonstrates that $S^2$ is a unbiased estimator of $\sigma^2$.\\
\vspace{0.1cm}
In the normal case(Section 6.5.2) the sample variance, $S^2$, is distributed as $\sigma^2 \chi_{n-1}^2/(n-1)$, and is independent of the sample mean. Thus(Section 6.5.2),
\vspace{0.1cm}
If $X_1,\cdots,X_n$ is iid $N(\mu,\sigma^2)$ then $\bar{X}_n \sim N(\mu,\frac{\sigma^2}{n}), \; S^2 \sim \sigma^2(\chi_{n-1}^2/(n-1))$, and $S^2$ and $\bar{X}_n$ are independent.\\
\vspace{0.1cm}
\underline{Comments}\\
\vspace{0.1cm}
(i) In the normal case, $\frac{X_i-\mu}{\sigma} \sim N(0,1)$ thus, $\sum\limits_1^n (X_i-\mu)^2 \sim \sigma^2(\sum\limits_1^n Z_i^2)\sim \sigma^2 \chi_n^2$. Replacing $\mu$ by $\bar{X}$ introduces a linear constraint, $\sum\limits_1^n (X_i-\bar{X})=\sum X_i -n(\frac{\sum X_i}{n})=0,$ and leads to a loss of a degree of freedom in the chi-square. 

\end{frame}

%%slide 8
\begin{frame}{Example}
In the case n=2,
$$(X_1-\bar{X})^2=(X_1-\frac{X_1+X_2}{x})^2=\frac{(X_1-X_2)^2}{4}$$
and,
$$(X_2-\bar{X})^2=(\frac{X_2-X_1}{2})^2 =\frac{(X_1-X_2)^2}{4}$$
thus,
$$\sum(X_i-\bar{X})^2 = (\frac{X_1-X_2}{\sqrt{2}})^2 =\sigma^2 N^2(0,1)=\sigma^2 \chi_1^2 =\sigma^2 \chi_{2-1}^2$$
(ii) Suppose that $Y_i=c+X_i, \; i=1,\cdots,n$ where c is an arbitrary constant. Then,
$$\bar{Y} =c+\bar{X} \text{ and } Y_i-\bar{Y} =(c+X_i)-(c+\bar{X})=(X_i-\bar{X})$$
so that,
$$(n-1) S_Y^2=\sum(Y_i-\bar{Y})^2=\sum (X_i-\bar{X})^2 =(n-1)S_X^2$$

\end{frame}

%%slide 9
\begin{frame}{Example}
Thus a shift in the mean is reflected in the sample mean, but leaves no change in the sample variance. This suggests that the sample mean does not provide information about the sample variance and is a plausibility argument for the independence result. However the proof does use normaility and doesn't hold in the general case.\\
\vspace{0.3cm}
\underline{Confidence interval for $\sigma^2$ in the normal case}\\
\vspace{0.3cm}
Since, $S^2 \sim \sigma^2 \chi_{n-1}^2 /(n-1)$, it follows that,
$$(4) \quad P(\chi_{n-1,1-\frac{\alpha}{2}}^2 \leq \frac{(n-1)S^2}{\sigma^2} \leq \chi_{n-1,\alpha/2}^2)=1-\alpha$$
where $P(\chi_{n-1}^2 > \chi_{n-1,\alpha/2}^2)=1-\frac{\alpha}{2}$. For example, if n=10 and $\alpha=0.05$ then from the table of chi-square percentiles in the appendix,
$$\chi_{9,0.975}^2=2.700, \quad \chi_{9,0.025}^2=19.023$$
19.023 is referred as the upper 0.025 percentile of $\chi_9^2$, while 2.700 the lower 0.025 percentile.\\

\end{frame}

%%slide 10
\begin{frame}{Example}
From (4) we transform to an equivalent inequality with $\sigma^2$ in the center:
$$a<\frac{(n-1)S^2}{\sigma^2}<b \Leftrightarrow \frac{1}{b} < \frac{\sigma^2}{(n-1)S^2}<\frac{1}{a} $$
$$\Leftrightarrow \frac{(n-1)S^2}{b}<\sigma^2 <\frac{(n-1)S^2}{a}$$
This gives us a $100(1-\alpha) \%$ confidence interval for $\sigma^2$:
$$(5) \quad Pr(\frac{(n-1)S^2}{\chi_{n-1,\alpha/2}^2} < \sigma^2 < \frac{(n-1)S^2}{\chi_{n-1,1-\alpha/2}^2})=1-\alpha$$
with $(n-1)S^2=\sum\limits_1^n (X_i-\bar{X})$\\
\vspace{0.3cm}
\underline{Example} n=10, $S^2=16.79, \; \alpha=0.05, \; \chi_{9,0.975}^2=2.700,\; \chi_{9,0.024}^2=19.023.$ The 95\% confidence interval for $\sigma^2$,

\end{frame}

%%slide 11
\begin{frame}{Example}
$$(\frac{9(16.79)}{19.023},\frac{9(16.79)}{2.700})=(7.943,55.967)$$
and for $\sigma$ we take square roots, so that the 95\% confidence interval for $\sigma$ equals,
$$(2.818,7.481)$$
\underline{Student t confidence interval for the mean}\\
\vspace{0.3cm}
We considered the case where $X_1,\cdots,X_n$ is iid $N(\mu,\sigma^2)$ with $\sigma^2=\sigma_0^2$ known. In practice the variance is almost always unknown. Here is the modification due to Gossett, who wrote under the pseudonym, Student.\\
\vspace{0.3cm}
Consider, $\frac{\sqrt{n} (\bar{X}_n -\mu)}{\sigma}$ which is $N(0,1)$. What happens if we replace $\sigma$ by S the sample standard deviation? Note that,
$$(4) \quad  \frac{\sqrt{n}(\bar{X}_n)-\mu}{S}=\frac{\frac{\sqrt{n} (\bar{X}_n -\mu)}{\sigma}}{\frac{S}{\sigma}} \sim \frac{N(0,1)}{\sqrt{\frac{\sigma^2 \chi_{n-1}^2/(n-1)}{\sigma^2}}}=\frac{N(0,1)}{\chi_{n-1}^2/(n-1)}$$



 


























\end{frame}

%%slide 12
\begin{frame}{Example}







\end{frame}

%%slide 13
\begin{frame}{Example}







\end{frame}

%%slide 14
\begin{frame}{Gamma Distribution}







\end{frame}


%%slide 15
\begin{frame}{Gamma Distribution}







\end{frame}
%%slide 16
\begin{frame}{Moment Generating Function}







\end{frame}
%%slide 17
\begin{frame}{Gamma Closure Result}









\end{frame}


%%slide 18
\begin{frame}{Exponential Distribution}






\end{frame}



%%slide 19
\begin{frame}{Chi-Square}







\end{frame}



%%slide 20
\begin{frame}{Chi-Square}







\end{frame}



%%slide 21
\begin{frame}{Chi-Square}







\end{frame}



%%slide 22
\begin{frame}{Exponential and Chi-Square}







\end{frame}



%%slide 23
\begin{frame}{Hazard function}






\end{frame}



%%slide 24
\begin{frame}{Hazard function}








\end{frame}



%%slide 25
\begin{frame}{Examples}






\end{frame}



%%slide 26
\begin{frame}{Examples}







\end{frame}

\end{document}





 