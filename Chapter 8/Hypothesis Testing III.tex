%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Overleaf (WriteLaTeX) Example: Molecular Chemistry Presentation
%
% Source: http://www.overleaf.com
%
% In these slides we show how Overleaf can be used with standard 
% chemistry packages to easily create professional presentations.
% 
% Feel free to distribute this example, but please keep the referral
% to overleaf.com
% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% How to use Overleaf: 
%
% You edit the source code here on the left, and the preview on the
% right shows you the result within a few seconds.
%
% Bookmark this page and share the URL with your co-authors. They can
% edit at the same time!
%
% You can upload figures, bibliographies, custom classes and
% styles using the files menu.
%
% If you're new to LaTeX, the wikibook is a great place to start:
% http://en.wikibooks.org/wiki/LaTeX
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\documentclass{beamer}

% For more themes, color themes and font themes, see:
% http://deic.uab.es/~iblanes/beamer\_gallery/index\_by\_theme.html
%
\mode<presentation>
{
  \usetheme{Madrid}       % or try default, Darmstadt, Warsaw, ...
  \usecolortheme{default} % or try albatross, beaver, crane, ...
  \usefonttheme{serif}    % or try default, structurebold, ...
  \setbeamertemplate{navigation symbols}{}
  \setbeamertemplate{caption}[numbered]
} 

\usepackage[english]{babel}
\usepackage[utf8x]{inputenc}
\usepackage{chemfig}
\usepackage[version=3]{mhchem}
\usepackage{tikz}
\usepackage{mathrsfs}
\usepackage{graphics,graphicx,picture}
\usepackage{setspace}
\usepackage{multirow}
\usepackage{amsmath}
% On Overleaf, these lines give you sharper preview images.
% You might want to `comment them out before you export, though.
\usepackage{pgfpages}
\pgfpagesuselayout{resize to}[%
  physical paper width=8in, physical paper height=6in]
\makeatletter
\newcommand{\rmnum}[1]{\romannumeral #1}
\newcommand{\Rmnum}[1]{\expandafter\@slowromancap\romannumeral #1@}
\newcommand{\Mypm}{\mathbin{\tikz [x=1.4ex,y=1.4ex,line width=.1ex] \draw (0.0,0) -- (1.0,0) (0.5,0.08) -- (0.5,0.92) (0.0,0.5) -- (1.0,0.5);}}%
\newcommand{\mypm}{\mathbin{\mathpalette\@mypm\relax}}
\newcommand{\@mypm}[2]{\ooalign{%
  \raisebox{.1\height}{$#1+$}\cr
  \smash{\raisebox{-.6\height}{$#1-$}}\cr}}
\makeatother

% Here's where the presentation starts, with the info for the title slide
\title{Hypothesis Testing III}
\author{Prof. Mark Brown}
\date{November 28, 2016}

\begin{document}

\begin{frame}
  \titlepage
\end{frame}

%%slide 2
\begin{frame}{Two-sided case}
\underline{Two-sided case}\\
\vspace{0.3cm}
Consider first, $N(\theta,\sigma_0^2), \theta$ unknown, $\sigma_0^2$ known. We wish to test,
$$H_0:\theta=\theta_0\quad vs \quad H_1: \theta\neq \theta_0$$
There does not exist a UMP level $\alpha$ case. For $\theta <\theta_0$ we maximize the power at $\theta$ by rejecting $H_0$ for $\bar{X} <\theta_0-\frac{\sigma_0 Z_\alpha}{\sqrt{n}}$, and for $\theta>\theta_0$ we maximize the power at $\theta$ by rejecting $H_0$ for $\bar{X} >\theta_0+\frac{\sigma_0 Z_\alpha}{\sqrt{n}}$. Since these are two different tests, we cannot maximize the power for both $\theta <\theta_0$ and $\theta>\theta_0$ by a single test.\\
\vspace{0.3cm}
In this situation the standard approach is to break up $\alpha$ into 2 equal pieces. We reject $H_0$ if either $\bar{X} >\theta_0+\frac{\sigma_0 Z_{\alpha/2}}{\sqrt{n}}$ or $\bar{X} <\theta_0-\frac{\sigma_0 Z_{\alpha/2}}{\sqrt{n}}$. This test can be shown to be ``UMP unbiased''. It maximizes the power at all $\theta$ among level $\leq \alpha$ tests which satisfy the additional requirement that $\pi_\theta=P_\theta(reject) \geq P_{\theta_0}(reject)$, for all $-\infty <\theta<\infty$.\\

\end{frame}

%%slide 3
\begin{frame}{Two-sided case}
We can also describe the above test by: reject $H_0$ if, $|Z|=\sqrt{n} (\frac{|\bar{X}-\theta_0|}{\sigma_0})$ $> Z_{\alpha/2}$. This test rejects $H_0$ at level $\alpha$ if and only if $\theta_0$ does not belong to the two-sided confidence interval, $\bar{X} \pm \frac{\sigma_0 Z_{\alpha/2}}{\sqrt{n}}$.\\
\vspace{0.2cm}
More generally if we have MLR in a statistic T then for testing $H_0: $ $\theta=\theta_0 \; vs \; H_1:\theta \neq \theta_0$, then we reject $H_0$ for $T>c_1$ or $T<c_2$, where $P_\theta(T>c_1)=P_\theta(T<c_2)=\frac{\alpha}{2}$(in the discrete case we try to get close to $\alpha/2$ for both probabilities).\\
\vspace{0.1cm}
We already considered, $N(\theta,\sigma_0^2)$ with $\sigma_0^2$ known. We will refer to that as example 1. Here are further examples.\\
\vspace{0.2cm}
2) $N(\theta,\sigma^2),\; \sigma^2$ unknown. Same 2-sided hypothesis as in example 1. Since,
$$|T|=\frac{\sqrt{n}(\bar{X}-\theta_0)}{S} \sim t_{n-1} \quad \text{under }\theta=\theta_0$$
we reject $H_0$ if, $|T|>t_{n-1,\alpha/2}$, equivalently if either \center $\bar{X}>\theta_0+\frac{S}{\sqrt{n}}t_{n-1,\alpha/2}$  or $\bar{X}<\theta_0-\frac{S}{\sqrt{n}}t_{n-1,\alpha/2}$

\end{frame}

%%slide 4
\begin{frame}{Example}
This 1 sample 2-sided t test is equivalent to rejecting $H_0$ at level $\alpha$ if $\theta_0$ does not lie in the $100(1-\alpha)\%$ 2 sided confidence interval for $\theta$,
$$\bar{X} \pm \frac{S}{\sqrt{n}} t_{n-1,\alpha/2} \quad (S^2=\frac{1}{n-1} \sum\limits_1^n (X_i-\bar{X})^2)$$
If we observe $T=t$, then the P-value is given by, $P(|T_{n-1}|>|t|)=$ $2Pr(T_{n-1}>|t|)$. In example 1 if we observe $Z=\frac{\sqrt{n}(\bar{X}-\theta_0)}{\sigma_0}=z$, then the P-value equals, $P(|Z|>|z|)=2P(Z>|z|)$.\\
\vspace{0.3cm}
3) $N(\theta,\sigma^2),\sigma^2$ unknown. We wish to test
$$H_0: \sigma^2=\sigma_0^2 \quad vs \quad H_1:\sigma^2 \neq \sigma_0^2$$
We reject $H_0$ if either $S^2$ is too large or too small, splitting the type 1 error $\alpha$ into two equals pieces. The test: Reject $H_0$ at level $\alpha$ if,
$$S^2 >\frac{\sigma_0^2 \chi_{n-1,\alpha/2}^2}{n-1} \text{ or } S^2 <\frac{\sigma_0^2 \chi_{n-1,1-\alpha/2}^2}{n-1}$$

\end{frame}

%%slide 5
\begin{frame}{Example}
Equivalently we reject $H_0$ at level $\alpha$ if and only if $\sigma_0^2$ does not lie in the $100(1-\alpha)\%$ two-sided confidence interval for $\sigma^2$, $(\frac{(n-1)S^2}{\chi_{n-1,\alpha/2}^2 },\frac{(n-1)S^2}{\chi_{n-1,1-\alpha/2}^2 })$\\
\vspace{0.3cm}
4) Expo($\theta$). $H_0:\theta=\theta_0 \; vs \; H_1:\theta\neq \theta_0$. We reject $H_0$ if either $S_n>$ $ \frac{\chi_{2n,\alpha/2}^2}{2\theta_0}$ or $S_n< \frac{\chi_{2n,1-\alpha/2}^2}{2\theta_0}$. This test is equivalent to rejecting $H_0$ at level $\alpha$ if and only if $\theta_0$ does not lie in the $100(1-\alpha)\%$ 2-sided confidence interval for $\theta$,
$$(\frac{\chi_{2n,1-\alpha/2}^2}{2S_n},\frac{\chi_{2n,\alpha/2}^2}{2S_n})$$
If we observe $S_n=s_n$ then the Pvalue equals,
$$2min(P(\chi_{2n}^2>2\theta_0 s_n),P( \chi_{2n}^2<2\theta_0 s_n))$$
4) Bernoulli trials. $H_0:\theta=\theta_0 \; vs \; H_1:\theta \neq \theta_0$. If we observe $N=k$ then the P-value equals,
$$2min[P_{\theta_0}(N \geq k),P_{\theta_0}(N \leq k)]$$

\end{frame}

%%slide 6
\begin{frame}{Example}
For a fixed $\alpha$ we reject $H_0$ at level $\alpha$ if and only if the P-value does not exceed $\alpha$.\\
\vspace{0.1cm}
\underline{Example} n=100, $\theta_0=0.6$, N=52. Then,
$$Pvalue=2min(Pr(Bin(100,0.6) \leq 52),Pr(Bin(100,0.6) \geq 52))$$
$$=2Pr(Bin(100,0.6)\leq 52)=0.1276$$
We accept $H_0$, for example, at the $\alpha=0.10$ level,\\
\vspace{0.1cm}
If in this same example, $N=69$, then the P-value equals, $2min(Pr($ $Bin(100,0.6) \leq 69),Pr(Bin(100,0.6) \geq 69))= 2Pr(Bin(100,0.6) \geq 69)=0.0797$. Thus we would reject $H_0$ at the $\alpha=0.10$ level, but not at $\alpha=0.05$.\\
\vspace{0.1cm}
5) \underline{2 sample, 2-sided t-test}.
$$X_1,\cdots,X_n, \; N(\mu_1,\sigma^2)$$
$$Y_1,\cdots, Y_m, \; N(\mu_2,\sigma^2)$$
(equal but unknown variances; $X's$ and $Y's$ independent)\\

\end{frame}

%%slide 7
\begin{frame}{Example}

Recall, $S_p^2=\frac{\sum (X_i-\bar{X})^2 +\sum(Y_i-\bar{Y})^2}{n+m-1}\sim \sigma^2 \frac{\chi_{n+m-2}^2}{n+m-2}$, and
 $$\frac{(\bar{X}-\bar{Y})-(\mu_1-\mu_2)}{S_p \sqrt{\frac{1}{n}+\frac{1}{m}}} \sim t_{n+m-2}$$
We wish to test:
$$H_0:\mu_1=\mu_2 \quad vs \quad H_1:\mu_1=\mu_2$$
$$\text{Under } H_0, \; T = \frac{(\bar{X}-\bar{Y})}{S_p \sqrt{\frac{1}{n}+\frac{1}{m}}} \sim t_{n+m-2}$$
The two sample two-sided level $\alpha$ t test for the above hypothesis rejects $H_0$ if either $T>t_{n+m-2,\alpha/2}$ or $T<-t_{n+m-2,\alpha/2}$ equivalently if $|T|>$ $t_{n+m-2,\alpha/2}$. This test is equivalent to rejecting $H_0$ at level $\alpha$ if and only if zero is not contained in the $100(1-\alpha)\%$ 2-sided confidence interval for $\mu_1-\mu_2$,
$$(\bar{X}-\bar{Y}) \pm S_p \sqrt{\frac{1}{n}+\frac{1}{m}}t_{n+m-2,\alpha/2}$$

\end{frame}

%%slide 8
\begin{frame}{Example}
6) Test for equality of variance.\\
\vspace{0.3cm}
$X_1,\cdots,X_n, \; N(\mu_1,\sigma_1^2),Y_1,\cdots, Y_m, \; N(\mu_2,\sigma_2^2), X's$ and $Y's$ independent, all parameters unknown. We wish to test,
$$H_0: \sigma_1^2 =\sigma_2^2 \quad vs \quad H_1:\sigma_1^2 \neq \sigma_2^2$$
Recall that $S_1^2 \sim \frac{\sigma_1^2 \chi_{n-1}^2}{n-1},S_2^2 \sim \frac{\sigma_2^2 \chi_{m-1}^2}{m-1}$ with $S_1^2, S_2^2$ independent. Thus,
$$\frac{S_1^2}{S_2^2} \sim \frac{\sigma_1^2}{\sigma_2^2} F_{n-1,m-1}$$
and under $H_0(\frac{\sigma_1^2}{\sigma_2^2}=1), \frac{S_1^2}{S_2^2} \sim F_{n-1,m-1}$. We reject $H_0$ if either $\frac{S_1^2}{S_2^2} >$ $ F_{n-1,m-1,\alpha/2}$ or $\frac{S_1^2}{S_2^2} < F_{n-1,m-1,1-\alpha/2}=\frac{1}{F_{m-1,n-1,\alpha/2}}$. This is equivalent to rejecting at level $\alpha$ if and only if 1 does not belong to the 2 sided confidence interval for $\frac{\sigma_1^2}{\sigma_2^2},(\frac{S_1^2}{S_2^2}F_{m-1,n-1,1-\alpha/2},\frac{S_1^2}{S_2^2}F_{m-1,n-1,\alpha/2})$.\\

\end{frame}

%%slide 9
\begin{frame}{Example}
If we observe, $\frac{S_1^2}{S_2^2}=x$ then the P-value equals,
$$2min(Pr(F_{n-1,m-1} \leq x),Pr(F_{n-1,m-1} \geq x))$$
8) Two sample exponential.\\
\vspace{0.2cm}
$$X_1,\cdots,X_n \sim Expo(\theta_1), \; Y_1,\cdots,Y_m \sim Expo(\theta_2)$$
with $X's$ and $Y's$ independent. Recall that,
$$2\theta_1 S_n \sim \chi_{2n}^2, \; 2\theta_2 T_m \sim \chi_{2m}^2 ,\; (S_n=\sum\limits_1^n X_i, T_m=\sum\limits_1^m Y_i)$$
We wish to test,
$$H_0:\theta_1=\theta_2 \quad vs \quad H_1:\theta_1 \neq \theta_2$$
at level $\alpha$. Under $H_0$,
$$\frac{S_n/n}{T_m/m}=\frac{\bar{X}}{\bar{Y}} \sim \frac{\chi_{2n}^2 /(2n)}{\chi_{2m}^2 /(2m)} \sim F_{2n,2m}$$

\end{frame}

%%slide 10
\begin{frame}{Comment}
We reject $H_0$ for either $\frac{\bar{X}}{\bar{Y}}> F_{2n,2m,\alpha/2}$ or $\frac{\bar{X}}{\bar{Y}}< F_{2n,2m,1-\alpha/2}=$ $\frac{1}{F_{2m,2n,\alpha/2}}$. This test is equivalent to rejecting $H_0$ at level $\alpha$ if and only if 1 does not belong to the $100(1-\alpha)\%$ 2-sided confidence interval for $\frac{\theta_1}{\theta_2}:(\frac{\bar{Y}}{\bar{X}} F_{2n,2m,1-\alpha/2},\frac{\bar{Y}}{\bar{X}} F_{2n,2m,\alpha/2})$.\\
\vspace{0.5cm}
\underline{Comment}
Some of the above testing situations weren't covered in my previous notes for their 1 sided versions. In example (2) the 1-sided t test($H_0:\theta=\theta_0(\text{or } \theta \leq \theta_0) \; vs \; H_1:\theta >\theta_0$) reject $H_0$ if
$$T=\frac{\sqrt{n}(\bar{X}-\theta_0)}{S} >t_{n-1,\alpha}$$
This is equivalent to rejecting if, $\bar{X} >\theta_0 +\frac{S}{\sqrt{n}}t_{n-1,\alpha}$. An obvious modification holds for $\theta \geq \theta_0 \; vs \; \theta <\theta_0$.\\
\vspace{0.3cm}

\end{frame}

%%slide 11
\begin{frame}{Comment}
In example (3) for testing $H_0:\sigma^2=\sigma_0^2(\text{or }\sigma^2 \leq \sigma_0^2) \; vs \; H_1:\sigma^2 >\sigma_0^2$, reject $H_0$ if $S^2 >\frac{\sigma_0^2 \chi_{n-1,\alpha}^2}{n-1}$,(with an obvious modification for the 1-sided test in the opposite direction).\\
\vspace{0.3cm}
In example (5), for testing, $H_0: \mu_1=\mu_2 (\text{or } \mu_1 \leq \mu_2) \; vs \; H_1: \mu_1>\mu_2$ we reject $H_0$ for
$$\bar{X}-\bar{Y} >S_p \sqrt{\frac{1}{n}+\frac{1}{m}} t_{n+m-2,\alpha}$$
In example (6), for testing $H_0: \sigma_1^2=\sigma_2^2(\text{or }\sigma_1^2 \leq \sigma_2^2) \; vs \; H_1: \sigma_1^2 >\sigma_2^2$, reject $H_0$ if 
$$\frac{S_1^2}{S_2^2} >F_{n-1,m-1,\alpha}$$

\end{frame}

\end{document}





 